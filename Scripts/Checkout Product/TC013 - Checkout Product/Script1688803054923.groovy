import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/TC001 - Login With Valid Username And Password'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('MyDemoApp/Cart/product'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnTambah'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnToCart'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnCartToCart'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnCheckout'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldFullname'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputFullname'), 'Monica Cindy')

Mobile.tap(findTestObject('MyDemoApp/Cart/tapAddress'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputAddress'), 'Kuningan')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldAdress2'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputAddress2'), 'Jawa Barat')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldCity'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputCity'), 'Kuningan')

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldState'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputState'), 'Indonesia')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldZipCode'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputFieldZipCode'), '45571')

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldCountry'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputCountry'), 'Indonesia')

Mobile.tap(findTestObject('MyDemoApp/Cart/btnPayment'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldUsernames'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputFullnames'), 'Monica Cindy')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldCardNumber'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputCardNumber'), '4275 1371 2278 128')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldExpiration'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputExpiredDate'), '03/25')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('MyDemoApp/Cart/tapFieldSecurity'), 0)

Mobile.sendKeys(findTestObject('MyDemoApp/Cart/inputSecurityCode'), '123')

Mobile.tap(findTestObject('MyDemoApp/Cart/btnReviewOrder'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnReviewOrder'), 0)

Mobile.verifyElementVisible(findTestObject('MyDemoApp/Cart/viewOtherCartColor'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/btnPlaceOrder'), 0)

Mobile.tap(findTestObject('MyDemoApp/Cart/checkoutComplete'), 0)

